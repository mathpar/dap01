package com.mathpar.NAUKMA.examples;

import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberZ;
import com.mathpar.number.NumberZp32;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

class MatrixMul8 {
    public static void main(String[] args)throws MPIException, IOException,ClassNotFoundException
    {
        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            int ord = 8;
            MatrixD A = new MatrixD(ord, ord, 10, ring);
            System.out.println("A = " + A);
            MatrixD B = new MatrixD(ord, ord,10, ring);
            System.out.println("B = " + B);
            MatrixD D = null;
            MatrixD[] AA = A.splitTo4();
            MatrixD[] BB = B.splitTo4();
            int tag = 0;
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[2]},0,2, 1, tag);
            MPITransport.sendObjectArray(new Object[] {AA[0], BB[1]},0,2, 2, tag);
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[3]},0,2, 3, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[0]},0,2, 4, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[2]},0,2, 5, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[1]},0,2, 6, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[3]},0,2, 7, tag);
            MatrixD[] DD = new MatrixD[4];
            DD[0] = (AA[0].multCU(BB[0], ring)).add((MatrixD) MPITransport.recvObject(1, 3),ring);
            DD[1] = (MatrixD) MPITransport.recvObject(2, 3);
            DD[2] = (MatrixD) MPITransport.recvObject(4, 3);
            DD[3] = (MatrixD) MPITransport.recvObject(6, 3);
            D = MatrixD.join(DD);
            System.out.println("RES = " + D.toString());
        }
        else
        {
            System.out.println("I'm processor " + rank);
            Object[] b = new Object[2];
            MPITransport.recvObjectArray(b,0,2,0, 0);
            MatrixD[] a = new MatrixD[b.length];
            for (int i = 0; i < b.length; i++)
                a[i] = (MatrixD) b[i];
            MatrixD res = a[0].multCU(a[1], ring);
            if (rank % 2 == 0)
            {
                MatrixD p = res.add((MatrixD) MPITransport.recvObject(rank + 1, 3), ring);
                MPITransport.sendObject(p, 0, 3);
            }
            else
            {
                MPITransport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }
}

